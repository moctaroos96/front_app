import React from 'react';
// import { render } from 'react-dom';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import Login from './pages/Login';
import Bienvenue from './pages/Bienvenue';
import SignUp from './pages/SignUp'

export const App = () => {
  return(
  <Router>
    <Route exact path="/" component={Login} />
    <Route path="/Bienvenue" component={Bienvenue} />
    <Route path="/register" component={SignUp} />
  </Router>
)
};
